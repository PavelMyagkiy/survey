<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['answer', 'survey_id'];

    public function surveys()
    {
        return $this->belongsTo(Survey::class);
    }
}

<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Question $question
     * @return void
     */
    public function index(Question $question)
    {
        Question::all();
        dd(Question::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Question $question
     * @return void
     */
    public function store(Request $request, Question $question)
    {

        //save checkbox
        if ($request->type == 1){
            //dd($request);
            $question = new Question();
            $question->survey_id = $request['survey_id'];
            $question->title= serialize($request['title']);
            $question->save();
        } else{
        // save text and radiobutton answer
            $question = new Question();
            $question->survey_id= $request['survey_id'];
            $question->title= $request['title'];
            $question->save();
        }
    }
}

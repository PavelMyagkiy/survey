<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['survey_id', 'title'];

    public function surveys()
    {
        return $this->belongsTo('\App\Survey');
    }
}

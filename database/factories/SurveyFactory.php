<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Survey;
use Faker\Generator as Faker;

$factory->define(Survey::class, function (Faker $faker) {
    return [
        'title' => $faker->text(20),
        'status' => $faker->randomElement($array = array(0,1)),
        'StartDate' => $faker->dateTime,
        'EndDate' => $faker->dateTime,

    ];
});
